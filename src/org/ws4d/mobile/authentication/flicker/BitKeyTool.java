package org.ws4d.mobile.authentication.flicker;

public class BitKeyTool {
	static final int LENGTH = 20; /* for now, only this is supported */
	static final int SYNC_LENGTH = 7; /* how many symbols for syncing */
	
	private Boolean[] mBoolKey = null;
	
	private KeyOutputDriver mDriver = null;
	
	/* define whether first bit is key's MSB or LSB */
	private Boolean mMSBFirst = true;
	
	public BitKeyTool() {
		
	}
	
	public BitKeyTool(KeyOutputDriver d) {
		this.setDriver(d);
	}
	
	public Boolean getMSBFirst() {
		return mMSBFirst;
	}

	public void setMSBFirst(Boolean msbFirst) {
		this.mMSBFirst = msbFirst;
	}

	public boolean intToBool(int key) {
		if (mBoolKey == null)
			mBoolKey = new Boolean[LENGTH+SYNC_LENGTH]; /* key length + sync-symbols */
		/* sync symbols */
		for (int i = 0; i < SYNC_LENGTH; i++) {
			mBoolKey[i] = ((i % 2) == 0);
		}
		
		/* key value */
		for (int i = 0 ; i < LENGTH; i++) {
			if (mMSBFirst)
				mBoolKey[SYNC_LENGTH+i] = ((key & (1 << (LENGTH - i - 1))) == (1 << (LENGTH - i - 1)));
			else
				mBoolKey[SYNC_LENGTH+i] = ((key & (1 << i)) == (1 << i));
		}
		
		return true;
	}
	
	public Boolean driveKeyOut() {
		if (mBoolKey == null)
			return false;
		if (mDriver == null)
			return false;
		return this.mDriver.driveKeyOut(mBoolKey);
	}

	public KeyOutputDriver getDriver() {
		return mDriver;
	}

	public void setDriver(KeyOutputDriver mDriver) {
		this.mDriver = mDriver;
	}
}
